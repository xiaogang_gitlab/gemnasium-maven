package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

func Match(path string, info os.FileInfo) (bool, error) {
	switch info.Name() {
	case "pom.xml", "build.gradle", "build.sbt":
		return true, nil
	default:
		return false, nil
	}
}

func init() {
	plugin.Register("gemnasium-maven", Match)
}
