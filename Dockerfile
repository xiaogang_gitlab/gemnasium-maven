FROM maven:3.6.0-jdk-8

ENV VRANGE_DIR="/vrange"
ARG GEMNASIUM_VRANGE_BRANCH="v2.3.0"
ARG GEMNASIUM_VRANGE_REPO="https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium"
ARG GEMNASIUM_VRANGE_URL="$GEMNASIUM_VRANGE_REPO/raw/$GEMNASIUM_VRANGE_BRANCH/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

ADD gemnasium-gradle-plugin-init.gradle /
ENV GRADLE_PLUGIN_INIT_PATH="/gemnasium-gradle-plugin-init.gradle"

ARG SBT_VERSION=1.3.4

RUN mkdir -p $VRANGE_DIR/semver && \
	wget -O $VRANGE_DIR/semver/vrange-linux $GEMNASIUM_VRANGE_URL/semver/vrange-linux && \
	chmod +x $VRANGE_DIR/semver/vrange-linux && \
	\
	# clone gemnasium-db
	git clone --branch $GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_LOCAL_PATH && \
    \
    # install sbt
    curl -L -o sbt-$SBT_VERSION.deb https://dl.bintray.com/sbt/debian/sbt-$SBT_VERSION.deb && \
    dpkg -i sbt-$SBT_VERSION.deb && \
    rm sbt-$SBT_VERSION.deb && \
    apt-get update && \
    apt-get install sbt && \
    sbt sbtVersion

# make sbt-dependency-graph plugin available to the project
ENV DEP_GRAPH_PLUGIN="addSbtPlugin(\"net.virtual-void\" % \"sbt-dependency-graph\" % \"0.10.0-RC1\")"
RUN mkdir -p ~/.sbt/1.0/plugins && \
    echo $DEP_GRAPH_PLUGIN > ~/.sbt/1.0/plugins/plugins.sbt

COPY analyzer /
CMD ["/analyzer", "run"]
