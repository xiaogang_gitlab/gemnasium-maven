# Gemnasium Maven analyzer changelog

## v2.9.0
- Add support for custom CA certs (!33)

## v2.8.0
- Bump gemnasium-maven-plugin to v0.4.0 (!35)

## v2.7.0
- Use `MAVEN_CLI_OPTS` with all invocations of maven (@fcbrooks) (!21)

## v2.6.0
- Add support for projects built with sbt (!20)

## v2.5.0
- Add support for projects built with gradle (!17)

## v2.4.0
- Add `MAVEN_CLI_OPTS` variable with `-DskipTests --batch-mode` default (!16)

## v2.3.0
- Use gemnasium-db git repo instead of the Gemnasium API (!15)

## v2.2.4
- Run `mvn install` before analysis to fix multi-modules support when using internal dependencies between modules

## v2.2.3
- Fix `DS_EXCLUDED_PATHS` not applied to dependency files (!13)

## v2.2.2
- Fix dependency list, include dependency files which do not have any vulnerabilities (!11)

## v2.2.1
- Sort the dependency files and their dependencies (!9)

## v2.2.0
- List the dependency files and their dependencies (!8)

## v2.1.2
- Bump common to v2.1.6
- Bump gemnasium to v2.1.2

## v2.1.1
- Fix multi-modules support

## v2.1.0
- Bump common to v2.1.4, introduce remediations
- Bump gemnasium to v2.1.1, introduce stable report order

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve vulnerability name, message and compare key

## v1.0.0
- Initial release
