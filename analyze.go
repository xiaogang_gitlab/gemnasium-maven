package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/ivy"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/mvnplugin"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
)

const (
	mavenPluginVersion  = "0.4.0"
	mavenPluginArtifact = "com.gemnasium:gemnasium-maven-plugin:" + mavenPluginVersion
	mavenPluginOutput   = "gemnasium-maven-plugin.json"
	mavenProjectFile    = "pom.xml"

	gradleProjectFile  = "build.gradle"
	gradleWrapper      = "gradlew"
	gradlePluginOutput = "gradle-dependencies.json"

	sbtProjectFile  = "build.sbt"
	sbtPluginOutput = "ivy-report.xml"

	flagVrangeDir            = "vrange-dir"
	flagVrangeMavenCmd       = "vrange-maven-cmd"
	flagMavenCliOpts         = "mavenCliOpts"
	flagGradlePluginInitPath = "gradlePluginInitPath"
)

func analyzeFlags() []cli.Flag {
	return append(
		scanner.Flags(),
		cli.StringFlag{
			Name:   flagVrangeDir,
			Usage:  "Path of the vrange directory",
			EnvVar: "VRANGE_DIR",
			Value:  "vrange",
		},
		cli.StringFlag{
			Name:   flagVrangeMavenCmd,
			Usage:  "vrange command for Maven",
			EnvVar: "VRANGE_MAVEN_CMD",
			Value:  "semver/vrange-" + runtime.GOOS + " maven",
		},
		cli.StringFlag{
			Name:   flagMavenCliOpts,
			Usage:  "Optional CLI arguments for the maven install command",
			Value:  "-DskipTests --batch-mode",
			EnvVar: "MAVEN_CLI_OPTS",
		},
		cli.StringFlag{
			Name:   flagGradlePluginInitPath,
			Usage:  "Optional CLI argument pointing to the init script for gemnasium-gradle-plugin",
			Value:  "gemnasium-gradle-plugin-init.gradle",
			EnvVar: "GRADLE_PLUGIN_INIT_PATH",
		},
	)
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	scan, err := scanner.NewScanner(c)
	if err != nil {
		return nil, err
	}

	if err := registerResolver(c); err != nil {
		return nil, err
	}

	projectFile, err := getProjectFile(path)
	if err != nil {
		return nil, err
	}

	switch projectFile {
	case gradleProjectFile:
		fmt.Println("Identified gradle project")
		err = buildGradle(c, path)
	case sbtProjectFile:
		fmt.Println("Identified sbt project")
		err = generateSbtDependencyGraph(c, path)
	default:
		fmt.Println("Identified maven project")
		err = buildMaven(c, path)
	}
	if err != nil {
		return nil, err
	}

	files, err := getDependencyOutputFiles(path)
	if err != nil {
		return nil, err
	}

	// scan dependency output
	result := []scanner.File{}
	for _, file := range files {
		// get the path to the module's dependency output file (in case of multi-module build, need distinct paths)
		relativeFilePath, err := filepath.Rel(path, file)
		if err != nil {
			return nil, err
		}
		depfilePath := filepath.Join(filepath.Dir(relativeFilePath), projectFile)
		depfile, err := scan.ScanFile(file, depfilePath)
		if err != nil {
			return nil, err
		}
		result = append(result, *depfile)
	}

	// return affected sources
	var output bytes.Buffer
	enc := json.NewEncoder(&output)
	enc.SetIndent("", "  ")
	enc.SetEscapeHTML(false)
	if err := enc.Encode(result); err != nil {
		return nil, err
	}

	return ioutil.NopCloser(&output), nil
}

func registerResolver(c *cli.Context) error {
	cmd := strings.SplitN(c.String(flagVrangeMavenCmd), " ", 2)
	path := filepath.Join(c.String(flagVrangeDir), cmd[0])
	args := cmd[1:]
	return vrange.RegisterCmd("maven", path, args...)
}

func getProjectFile(path string) (string, error) {
	for _, filename := range []string{gradleProjectFile, mavenProjectFile, sbtProjectFile} {
		if _, err := os.Stat(filepath.Join(path, filename)); err == nil {
			return filename, nil
		}
	}
	return "", fmt.Errorf("no compatible project found at %s", path)
}

// buildGradle builds a gradle project at the given and outputs a dependency report (using gemnasium-gradle-plugin)
func buildGradle(c *cli.Context, path string) error {
	wrapper, err := findGradleWrapper(path)
	if err != nil {
		return err
	}
	return setupCmd(path, exec.Command(wrapper, "--init-script", c.String(flagGradlePluginInitPath), "gemnasiumDumpDependencies")).Run()
}

func findGradleWrapper(path string) (string, error) {
	gradlewPath := filepath.Join(path, gradleWrapper)
	if _, err := os.Stat(gradlewPath); err != nil {
		return "", err
	}
	return gradlewPath, nil
}

// buildMaven builds a maven project at the given path and then dumps out the generated
// dependencies (using gemnasium-maven-plugin) into a report parsable by gemnasium
func buildMaven(c *cli.Context, path string) error {
	opts := c.String(flagMavenCliOpts)

	// install gemnasium-maven-plugin
	installArgs := []string{"org.apache.maven.plugins:maven-dependency-plugin:get", "-Dartifact="+mavenPluginArtifact}
	installArgs = append(installArgs, strings.Fields(opts)...)
	installPlugin := setupCmd(path, exec.Command("mvn", installArgs...))
	if err := installPlugin.Run(); err != nil {
		return err
	}

	// build project to make sure internal deps are built
	buildArgs := []string{"install"}
	buildArgs = append(buildArgs, strings.Fields(opts)...)
	buildProject := setupCmd(path, exec.Command("mvn", buildArgs...))
	if err := buildProject.Run(); err != nil {
		return err
	}

	// run gemnasium-maven-plugin
	dumpArgs := []string{"com.gemnasium:gemnasium-maven-plugin:dump-dependencies"}
	dumpArgs = append(dumpArgs, strings.Fields(opts)...)
	dumpDeps := setupCmd(path, exec.Command("mvn", dumpArgs...))
	return dumpDeps.Run()
}

// generateSbtDependencyGraph analyzes the dependencies at the given project path and then outputs an ivy report
// (using sbt-dependency-graph plugin)
func generateSbtDependencyGraph(c *cli.Context, path string) error {
	if err := setupCmd(path, exec.Command("sbt", "ivyReport")).Run(); err != nil {
		return err
	}

	return writeIvyReport(path)
}

func setupCmd(path string, cmd *exec.Cmd) *exec.Cmd {
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd
}

func getDependencyOutputFiles(path string) ([]string, error) {
	var matches []string

	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		switch info.Name() {
		case gradlePluginOutput, mavenPluginOutput, sbtPluginOutput:
			matches = append(matches, path)
		}
		return nil
	})

	return matches, err
}
